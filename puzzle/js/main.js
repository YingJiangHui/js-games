const GAME_WIDTH = 600, GAME_HEIGHT = 600;
var imgUrl = "",
    num = 3,
    blockW = GAME_WIDTH / 3,
    gapBlock = null,
    can_move = true,
    once=true,
    timer=null,
    on_off = true,
    can_rank=true,
    s = 0, m = 0;
$('.frame-game,.frame-ranking,.frame-difficulty').hide();
blockBg(120/3,3,".image-sample1");
blockBg(120/4,4,".image-sample2");
blockBg(120/6,6,".image-sample3");

$('.btn-stop').on('click',function () {
    popup("点击开始继续游戏","restart",300,200)
    clearInterval(timer);
    mask();
    $('.clock').css({
        animationPlayState:"paused",
    })
})

$('body').on('click', '.block', function () {
    console.log($(this))
    if ($(this).is('.active')) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active').siblings().removeClass('active')
    }
})

$('.btn-H').unbind('click').on('click',function () {
    init();
    checkPage($('.frame-ranking'),$('.frame-home'))
})

$('.btn-R').on('click',function () {
    checkPage($('.frame-game'),$('.frame-home'))
    init();
})

$('.btn-description').on('click',function () {
    popup("description","close",500,350)
})

$('body').on('transitionstart', '.block', function () {
    can_move = false;
}).on('transitionend', '.block', function () {
    can_move = true;
    if (finish()){
        clearInterval(timer)
        ranking()
        // tip('You Win')
        $(gapBlock).css({
            animation: 'gap-in 1s linear',
            top: `${(num-1)*blockW}px`,
            left: `${(num-1)*blockW}px`
        }).one('animationend',function () {
            checkPage($('.frame-game'),$('.frame-ranking'))
        }).appendTo('.content')
    }

})

$('.btn-start-game').on('click', function () {
    let text = $(this).text();
    switch (text) {
        case "Easy":
            num = 3;
            blockW = GAME_WIDTH / num;
            tagIn('Easy')
            break;
        case "Normal":
            tagIn('Normal')
            num = 4
            blockW = GAME_WIDTH / num;
            break;
        case "Hard":
            tagIn('Hard')
            num = 6;
            blockW = GAME_WIDTH / num;
            break;

    }
    blockBg(blockW,null,".content")
    createImage();
    can_move = false;
    checkPage($('.frame-difficulty'), $('.frame-game'));
})

//监听input事件
$('.files').on('input', function () {
    var file = $('.files')[0].files[0]
    if (!/image\/\w+/.test(file.type)) {
        tip('file is not image', '#f68a87');
        return false;
    }
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        imgUrl = reader.result;
        console.log(imgUrl)
        $('.image').css({
            background: `url("${imgUrl}") no-repeat center/cover`,
            animation: 'image-in 0.5s linear',
            color: 'transparent',
        })
    }
})

$('.image').on('click', function () {
    $('.files').click();
})

$('.btn-user').on('click', function () {
    // if ($('.files').val() == ''||$('.ipt-username').val() == '') {
    //     tip('no', '#f68a87');
    //     return false
    // }
    // $('.name').text("$('.ipt-username').val()");
    $('.name').text("Anonymous Player");
    checkPage($('.frame-home'), $('.frame-difficulty'));
})

$(document).unbind('keyup').on('keyup', function (e) {
    let kc = e.keyCode;
    if ($('.active').length && can_move) {
        control($('.active'),kc)
    } else if (kc == 37 || kc == 38 || kc == 39 || kc == 40 && can_move) {
        setTimeout(function () {
            once=true;
        },200)
        if (once){
            once=false;
            control(searchCanMoveDom(kc), kc)
        }
    }
})


function init() {
    imgUrl = "",
        num = 3,
        blockW = GAME_WIDTH / 3,
        gapBlock = null,
        can_move = true,
        once = true,
        timer = null,
        on_off = true,
        can_rank = true,
        s = 0, m = 0;
    $('canvas').remove();
    $('.ipt-username').val('');
    $('.image').css({ background: '', color: 'black', animation: '' })
    tagOut();
    $('.files').val('')
    $('.clock').css({ animation: '' });
    $('.time').text('00:00');
}

function control(dom, kc) {
    let left = parseInt(dom.css('left')),
        top = parseInt(dom.css('top'));
    switch (kc) {
        case 37:
            if (left > 0 && can_move && !checkBlockExist(left - blockW, top)) {
                $(dom).css("left", `${left - blockW}px`)
            }
            break;
        case 38:
            if (top > 0 && can_move && !checkBlockExist(left, top - blockW)) {
                $(dom).css("top", `${top - blockW}px`)
            }
            break;
        case 39:
            if (left < GAME_WIDTH - blockW && can_move && !checkBlockExist(left + blockW, top)) {
                $(dom).css("left", `${left + blockW}px`)
            }
            break;
        case 40:
            if (top < GAME_HEIGHT - blockW && can_move && !checkBlockExist(left, top + blockW)) {
                $(dom).css("top", `${top + blockW}px`)
            }
            break;
    }
}

function searchCanMoveDom(kc) {
    let dom = null
    $('.block').each(function() {
        let left = parseInt($(this).css('left')),
            top = parseInt($(this).css('top'))
        console.log(blockW)
        switch (kc) {
            case 37:
                if (left > 0 && can_move && !checkBlockExist(left - blockW, top)) {
                    dom = this
                }
                break;
            case 38:
                if (top > 0 && can_move && !checkBlockExist(left, top - blockW)) {
                    dom = this
                }
                break;
            case 39:
                if (left < GAME_WIDTH - blockW && can_move && !checkBlockExist(left + blockW, top)) {
                    dom = this
                }
                break;
            case 40:
                if (top < GAME_HEIGHT - blockW && can_move && !checkBlockExist(left, top + blockW)) {
                    dom = this
                }
                break;
        }
    })
    console.log(dom);
    return $(dom)
}

function checkBlockExist(left, top) {
    let exist = false;
    $(".block").each(function() {
        if (parseInt($(this).css('left')) == left && parseInt($(this).css('top')) == top) {
            //存在返回true
            exist = true;
        }
    });
    return exist;
}

function createImage() {
    let canvas = $(`<canvas class="canvas"></canvas>`)[0];
    let img = new Image(),
        pos = 0,
        width = 0;
    canvas.width = GAME_WIDTH;
    canvas.height = GAME_HEIGHT;
    img.src = imgUrl;
    img.onload = function() {
        pos = (Math.max(img.width, img.height) - Math.min(img.width, img.height)) / 2
        width = Math.min(img.width, img.height)
        let ctx = canvas.getContext('2d');
        if (img.width > img.height) {
            ctx.drawImage(img, pos, 0, width, width, 0, 0, canvas.width, canvas.height);
        } else {
            ctx.drawImage(img, 0, pos, width, width, 0, 0, canvas.width, canvas.height);
        }
        let originalArr = cutBlock(ctx);
        let fillterArr = cutBlock(ctx);
        gapBlock = fillterArr.pop();
        console.log(fillterArr)
        let randomArr = randomSort(fillterArr);
        renderingDom(originalArr, function() {
            $('canvas').remove();
            renderingDom(randomArr, function() {
                times();
                can_move = true;
                $('.clock').css({
                    animation: "clock 60s linear infinite",
                })
            })
        })
    }
}

function cutBlock(ctx) {
    let block = [];
    for (let i = 0; i < num; i++) {
        for (let j = 0; j < num; j++) {
            let ctxBlock = $(`<canvas class="block" data-row='${i}' data-col='${j}'></canvas>`)[0]
            ctxBlock.width = blockW - 5;
            ctxBlock.height = blockW - 5;
            let imgBlock = ctx.getImageData(ctxBlock.width * j, ctxBlock.height * i, ctxBlock.width, ctxBlock.height);
            ctxBlock.getContext('2d').putImageData(imgBlock, 0, 0);
            block.push(ctxBlock);
        }
    }
    return block;
}

function randomSort(arr) {
    let i = arr.length;
    while (i) {
        let j = Math.floor(Math.random() * i--);
        [arr[j], arr[i]] = [arr[i], arr[j]];
    }
    return arr
}

function renderingDom(arr, callback) {
    let k = 0,
        keyframe = "";
    if (on_off) {
        keyframe = 'block-out 1s ease-in'
        console.log(gapBlock);
        /*这里不能直接使用gapBlock*/

        on_off = false;
    } else {
        keyframe = 'block-in 0.5s linear'
    }
    for (let i = 0; i < num; i++) {
        for (let j = 0; j < num; j++) {
            $(arr[k++]).css({
                top: `${blockW * i}px`,
                left: `${blockW * j}px`,
                animation: `${keyframe}`,
            }).one('animationend', function() {
                callback && callback();
            }).appendTo('.content')
        }
    }
    $(arr[num * num - 1]).css({
        animation: 'gap-out 1s linear'
    })

}

function checkPage(current, next, callback) {
    current.css({
        animation: 'frame-out 0.5s',
        //    注意使用one绑定
    }).one('animationend', function() {
        current.css('animation', '').hide();
        next.css({
            animation: 'frame-in 0.5s',
        }).show().one('animationend', callback)
    })
}

function finish() {
    let can_win = false,
        number = 0
    $('.block').each(function() {
        console.log(this.getAttribute("data-col"));
        let left = parseInt($(this).css('left')),
            top = parseInt($(this).css('top'))
        if (left == $(this).attr('data-col') * blockW && top == $(this).attr('data-row') * blockW) {
            number++
        }
    })
    number == num * num - 1 ? can_win = true : can_win = false;
    return can_win
}

function ranking() {
    if (!can_rank) {
        return false
    }
    can_rank = false;
    let rank = [],
        time = $('.time').text(),
        name = $('.name').text(),
        difficult = $('.tag').text();
    $('.ranking-content').html("");
    if (localStorage.difficulty) {
        rank = JSON.parse(localStorage.difficulty);
    }
    rank.push({
        name: name,
        time: time,
        difficult: difficult,
    });
    let filterArr = rank.filter(function(arr) {
        return arr.difficult == difficult;
    })
    filterArr.sort(function(a, b) {
        return parseInt(a.time.toString().replace(":", '')) - parseInt(b.time.toString().replace(":", ''))
    });
    console.log(filterArr)
    for (let i in filterArr) {
        $(`<ul><li>${i * 1 + 1}</li><li>${filterArr[i].name}</li><li>${filterArr[i].time}</li><li>${filterArr[i].difficult}</li></ul>`).appendTo('.ranking-content')
    }
    localStorage.difficulty = JSON.stringify(rank);
}

function tagIn(text) {
    $(`<div class="tag">${text}</div>`).css({
        animation: 'tag-in 1s',
    }).appendTo('body');
}

function tagOut() {
    $('.tag').css({
        animation: 'tag-out 1s'
    }).one('animationend', function() {
        $(this).remove();
    })
}

function times() {
    clearInterval(timer);
    timer = setInterval(function() {
        if (s == 60) {
            s = 0
            m++
        }
        $('.time').text(templateTime(++s, m))
    }, 1000)
}

function templateTime(s, m) {
    let ts = '',
        tm = ''
    s >= 10 ? ts = `${s}` : ts = `0${s}`;
    m >= 10 ? tm = `${m}` : tm = `0${m}`;
    return tm + ":" + ts
}

function tip(text, bg) {
    bg = bg || "#f68a87"
    $(`<div class="tip">${text}</div>`).css({
        background: `${bg}`,
        animation: 'tip 1s linear'
    }).appendTo('body').one('animationend', function() {
        $(this).remove()
    });
}

function mask() {
    $(`<div class="mask"></div>`).css({
        animation: "mask-in 0.5s",
    }).appendTo('body');
}

function popup(text, btn_text, w, h) {
    $(`<div class="popup"><div class="popup-title">提示</div><div class="popup-content"><p>${text}</p><button class="popup-btn">${btn_text}</button></div></div>`).css({
        animation: "popup-in 0.5s",
        width: `${w}px`,
        height: `${h}px`,
    }).appendTo('body');
    $('.popup-btn').on('click', function() {
        $('.mask').css({
            animation: 'mask-out 0.5s linear',
        }).one('animationend', function() {
            $(this).remove();
        })
        $('.popup').css({
            animation: 'popup-out 0.5s linear',
        }).one('animationend', function() {
            $(this).remove();
            times()
            $('.clock').css({
                animationPlayState: 'running'
            })
        })
    })
}

function blockBg(w, n, addDom) {
    n = n || num;
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n; j++) {
            $('<div class="block-bg"></div>').css({
                width: `${w-5}px`,
                height: `${w-5}px`,
                top: `${w*i}px`,
                left: `${w*j}px`,
            }).appendTo(addDom);
        }
    }
}
class Player extends Item{
    constructor(){
        super();
        //解构赋值
        [this.l,this.t,this.r,this.b]=[false,false,false,false];
        this.dom=$(`<div class="player item"></div>`);
        this.left=30;
        this.top=260;
        this.spawn();
        this.speed=10;
    }
    action(){
        this.l && this.getLeft()>10 && this.dom.css({'left':`-=${this.speed}px`,'transform':"rotate(0deg)"});
        this.t && this.getTop()>10 &&this.dom.css({'top':`-=${this.speed}px`,'transform':"rotate(-3deg)"});
        this.r && this.getLeft()<850 && this.dom.css({'left':`+=${this.speed}px`,'transform':"rotate(0deg)"});
        this.b && this.getTop()<550 && this.dom.css({'top':`+=${this.speed}px`,'transform':"rotate(3deg)"});
        for (let i in dataCenter.items){
            let item=dataCenter.items[i];
            if (!item.dom.is('.player')&&!item.dom.is('bullet')){
                if (this.impact(item)){
                    if (item.dom.is('.fuel')){
                        dataCenter.setWithFuel(15)
                    }else if(item.dom.is('.enemy')||item.dom.is('.enemy-bullet')||item.dom.is('.meteorite')){
                        dataCenter.setWithFuel(-15)
                        $('.game-frame').css("animation","collision 1s linear").one("animationend",()=>{
                            $('.game-frame').css("animation",'none')
                        })
                    }else{
                        dataCenter.score-=2;
                    }
                    item.die();
                }
            }
        }
    }
    shot(){
        // if (dataCenter.musicAction){
            dataCenter.game.musicPlay("./sound/shoot.mp3");
        // }
        new Bullet(this.getLeft(),this.getTop(),'player')
    }
}

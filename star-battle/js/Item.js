class Item {
    constructor() {
        this.left = 0;
        this.top = 0;
        this.dom = $(``);
        //随机生成的id
        this.id = Math.random();
        this.speed = 15 - Math.random() * 3;
    }

    //生产元素
    spawn() {
        //谁调用就this就是谁
        dataCenter.items[this.id] = this;
        this.dom.css({
            'left': this.left,
            'top': this.top,
        }).one('animationend', () => {
            //动画结束的时候执行die函数（当然时在有动画的前提下，如自己的宇宙飞船并没有动画）
            this.die();
        }).appendTo('.game-frame')
    }
    /**
     * 物体碰撞检测
     * @param {对方元素} othor 
     * @returns 
     */
    impact(othor) {
        return this.getLeft() < othor.getLeft() + othor.getWidth() && this.getLeft() + this.getWidth() > othor.getLeft()
            && this.getTop() < othor.getTop() + othor.getHeight() && this.getTop() + this.getHeight() > othor.getTop()
    }

    getLeft() {
        let game_l = $('.game-frame').offset().left;
        return parseInt(this.dom.offset().left) - game_l
    }

    getTop() {
        let game_2 = $('.game-frame').offset().top;
        return parseInt(this.dom.offset().top) - game_2
    }

    //被子类继承谁调用它，this就指向谁
    getWidth() {
        return parseInt(this.dom.css('width'))
    }

    getHeight() {
        return parseInt(this.dom.css('height'))
    }

    die() {
        this.dieAnimation();
        delete dataCenter.items[this.id];
        this.dom.remove();
    }
    /**
     * 死亡动画
     */
    dieAnimation() {
        $(`<div class="boom" style="animation: item-enemy-animation steps(4) 0.5s;"></div>`).css({
            left:this.getLeft(),
            top:this.getTop(),
        }).appendTo('.game-frame').one('animationend',function () {
            $(this).remove();
        });
    }
    /**
     * 
     */
    action() {
    }
}

class meteorite extends Item {
    constructor() {
        super()
        this.dom = $(`<div class="meteorite item" style="animation:r-l ${this.speed}s linear"></div>`)
        this.left = 1000;
        this.top = Math.random() * 400;
        this.speed = 9 + Math.random() * 3;
        this.blood=2
        this.spawn();
    }

    dieAnimation() {

    }
}

class Enemy extends Item {
    constructor() {
        super()
        this.left = 1000;
        this.top = Math.random() * 520;
        this.dom = $(`<div class="enemy item" style="animation: item-enemy-animation steps(4) 0.4s infinite,r-l ${this.speed}s linear "></div>`)
        this.num = 2;
        this.spawn()
    }

    action() {
        if (this.getLeft() < 700 && this.num == 2) {
            this.shot();
        }
        if (this.getLeft() < 600 && this.num == 1) {
            this.shot();
        }
    }

    shot() {
        this.num--;
        new Bullet(this.getLeft(), this.getTop(), 'enemy');
    }
}

class Bullet extends Item {
    /**
     * @param bulletLeft子弹位置的初始Left值
     * @param bulletTop初始Top值
     * @param direction子弹方向
     */
    constructor(bulletLeft, bulletTop, type) {
        super()
        this.top = bulletTop + 15;
        this.type = type;
        this.dom = $(`<div class="${type}-bullet bullet item"></div>`);
        if (type == 'player') {
            this.left = bulletLeft + 100;
            this.direction = 'l-r';
            this.speed = 1;
            this.dom = $(`<div class="${type}-bullet bullet item" style="animation:${this.direction} ${this.speed}s linear;background: #7983ff"></div>`);

        } else {
            this.left = bulletLeft - 10;
            this.direction = 'r-l';
            this.speed = 6;
            this.dom = $(`<div class="${type}-bullet bullet item" style="animation:${this.direction} ${this.speed}s linear;background: #FF7B79"></div>`);
        }
        this.spawn();
    }

    dieAnimation() {
    }

    action() {
        for (let i in dataCenter.items) {
            let item = dataCenter.items[i];
            //过滤掉燃油，和子弹
            //.is()方法是一个jquery方法，它的作用是判断元素是否是指定某元素，
            // 使用的时候也是和jquery选择器类似用
            if (!item.dom.is('.bullet') && !item.dom.is('.fuel')) {
                if (this.type == 'player') {
                    if (!item.dom.is('.player')) {
                        if (this.impact(item)) {
                            // if (dataCenter.musicAction){
                            // }
                            if (item.dom.is('.enemy')) {
                                dataCenter.score += 2;
                            } else if(item.dom.is('.meteorite')) {
                                item.blood--
                                if(item.blood===0){
                                    dataCenter.score += 10;
                                }else{
                                    this.die();
                                    return
                                }
                            }else{
                                dataCenter.score -= 10;
                            }
                            item.die();
                            this.die();
                            dataCenter.game.musicPlay("./sound/destroyed.mp3");
                        }
                    }
                } else {
                    if (!item.dom.is('.enemy') && !item.dom.is('.meteorite')) {
                        if (this.impact(item)) {
                            this.die();
                        }
                    }
                }
            }


        }
    }
}
class Friend extends Item {
    constructor(){
        super();
        this.left = 1000;
        this.top = Math.random() * 520;
        this.speed=6+Math.random()*3;
        this.dom=$(`<div class="friend item" style="animation: item-enemy-animation steps(4) 0.5s infinite, r-l ${this.speed}s linear"></div>`);
        this.spawn();
    }
    action(){
        for (let i in dataCenter.items){
            let item=dataCenter.items[i];
            if (item.dom.is('.enemy')){
                if (this.impact(item)){
                    this.die();
                    item.die();
                }
            }
        }
    }
}

class Fuel extends Item {
    constructor() {
        super();
        this.left = Math.random() * 960;
        this.top = -20;
        this.dom = $(`<div class="fuel item" style="animation: t-b ${this.speed}s linear normal;"></div>`);
        this.spawn();
    }
    dieAnimation() {

    }
}



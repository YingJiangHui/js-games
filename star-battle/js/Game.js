//1油量和计分版如何快速显示
//2小鸟死亡掉落效果
//3
class Game {
    constructor() {
        this.init();
    }

    //初始化
    init() {
        //循环删除所有的项
        for (let i in dataCenter.items) {
            dataCenter.items[i].die();
        }
   
        dataCenter = {
            game: '',
            player: '',
            fuel: 15,
            maxFuelCapacity: 60,
            items: {},
            time: 0,
            score: 0,
            fontSize: 16,
            flag: false,
            musicAction: true,
            setWithFuel(fuel){
                dataCenter.fuel=Math.min(dataCenter.fuel+fuel,dataCenter.maxFuelCapacity)
            }
        };

        dataCenter.game = this;
        this.num=0;
        this.bgm = $(".bgm")[0];
        this.muted = false;
        $('.music').css({background: "#7983ff url('./img/music-pause.png') no-repeat center"});

        $('.game-frame,.over-frame,.ranking').hide();
        $('.inputName').show();
        $('.userName').val('');
        $(".start-frame").show();

        // $(".start-frame").css({
        //     'animation':'frame-in 1s'
        // }).one('animationend',()=>{

        // });

        this.event();
    }

    //事件
    event() {
        $('.music').unbind('click').on('click', () => {
            if (dataCenter.flag) {
                this.music();
            }
        })
        $('.font-size-plus').unbind('click').on('click', () => {
            $("html").css("fontSize", `${dataCenter.fontSize < 22 ? ++dataCenter.fontSize : dataCenter.fontSize = 22}px`);
        })
        $('.font-size-cut').unbind('click').on('click', () => {
            $("html").css("fontSize", `${dataCenter.fontSize > 10 ? --dataCenter.fontSize : dataCenter.fontSize = 10}px`);

        })
        $('.goBackHome').unbind('click').on('click', () => {
            this.checkPage($('.game-over'), $('.start-frame'),()=>{
            });
        })
        $('.goHome').unbind('click').on('click', () => {
            this.checkPage($('.over-frame'), $('.start-frame'));
        })
        $('.continue').unbind('click').on('click', () => {
            if ($('.userName').val() == '') {
                this.tip("userName is null");
                return false;
            }
            let _this = this
            $.ajax({
                url: '../register.php',
                type: 'post',
                data: {
                    name: $(".userName").val(),
                    time: dataCenter.time,
                    score: dataCenter.score,
                },
                dataType: 'json',
                success(val) {
                    _this.ranking(val);
                }
            });

            this.checkPage($('.inputName'), $('.ranking'))
        })
        $('.gameStop').unbind('click').on('click', () => {
            this.stop()
        })
        $('.start-btn').unbind('click').on('click', () => {
            this.init();
            dataCenter.player = new Player();

            this.checkPage($('.start-frame'), $('.game-frame'),()=>{
                dataCenter.flag=true
                this.intervalTimer();
                this.bgm.play();
            });
        })
        $(document).unbind('keydown').unbind('keyup').on('keydown', (e) => {
            if (dataCenter.flag) {

                var e = e || window.e;
                let kc = e.keyCode;
                switch (kc) {
                    case 37:
                        dataCenter.player.l = true;
                        $('<style>.player::after{opacity: 1;transform: scale(0.5)}</style>').appendTo('.game-frame');
                        break;
                    case 38:
                        dataCenter.player.t = true;
                        break;
                    case 39:
                        dataCenter.player.r = true;
                        break;
                    case 40:
                        dataCenter.player.b = true;
                        break;
                }
                if (kc == 40 || kc == 38 || kc == 39) {
                    $('<style>.player::after{opacity: 1;transform: scale(1)}</style>').appendTo('.game-frame');
                }
            }
        }).on('keyup', (e) => {
            if (dataCenter.flag) {
                let kc = e.keyCode;
                if (kc == 40 || kc == 38 || kc == 39 || kc == 37) {
                    $('<style>.player::after{opacity: 0}</style>').appendTo('.game-frame');
                }
                switch (kc) {
                    case 37:
                        dataCenter.player.l = false;
                        break;
                    case 38:
                        dataCenter.player.t = false;
                        break;
                    case 39:
                        dataCenter.player.r = false;
                        break;
                    case 40:
                        dataCenter.player.b = false;
                        break;
                    case 32:
                        dataCenter.player.shot();
                }
            }
        })
    }

    intervalTimer() {
        clearInterval(this.timer);
        clearInterval(this.check);
        this.timer = setInterval(() => {
            dataCenter.time++;
            dataCenter.fuel--;

           this.simple()
        }, 1000);

        this.check = setInterval(() => {
            this.gameScoring();
            for (let i in dataCenter.items) {
                dataCenter.items[i].action()
            }
        }, 15)
    }

    difficulty(){
        new Enemy()
        new Enemy()
        if (this.num % 3 == 1) {
            //每三秒生成汽油
            new Friend();
            new Fuel();
            new meteorite();
        }
        this.num++;
    }
    commonly(){
        new Enemy()
        if (this.num % 3 == 1) {
            //每三秒生成汽油
            new Friend();
            new Fuel();
            new meteorite();
        }
        this.num++;
    }
    simple(){
        //每秒生成敌人
        this.num % 2 == 1 ? new Enemy() : null;
        if (this.num % 3 == 1) {
            //每三秒生成汽油
            new Friend();
            new Fuel();
            new meteorite();
        }
        this.num++;
    }

    //音乐
    music() {
        if (!this.muted) {
            this.bgm.pause();
            $('.music').css({background: "#7983ff url('./img/music-play.png') no-repeat center"});
        } else {
            this.bgm.play()
            $('.music').css({background: "#7983ff url('./img/music-pause.png') no-repeat center"});
        }
        this.muted = !this.muted;
    }

    tryToOpenMusic(){
        if (!this.muted) {
            this.bgm.play()
        }
    }

    gameScoring() {
        if (dataCenter.fuel <= 0) {
            dataCenter.fuel = 0;
            $('.fuelGauge .fuelGaugeCapacity').unbind('transitionend').one('transitionend',()=>{
                console.log('end');
                this.gameOver();
            })
            
            //游戏结束
        }
        $('.score').html(`${dataCenter.score}`);
        $('.time').html(`${dataCenter.time}`);
        $('.fuelGauge span').html(`${dataCenter.fuel}`);
        $('.fuelGauge .fuelGaugeCapacity').css({
            width: dataCenter.fuel/dataCenter.maxFuelCapacity*100+'%',
        })
    }

    gameOver() {
        dataCenter.flag=false
        this.bgm.pause()    
        this.checkPage($('.game-frame'), $('.game-over'),()=>{
            
        });
        clearInterval(this.timer);
        clearInterval(this.check);
        
    }

    ranking(rank) {
        let user = [];
        $('.list-content').html('');
        for (let i in rank) {
            let name = rank[i].name, time = rank[i].time, score = rank[i].score;
            user.push({
                Name: name,
                Score: score,
                Time: time,
            });
            user.sort(function (a, b) {
                return parseInt(b.Score) - parseInt(a.Score);
            });
        }
        for (let i = 0; i < user.length; i++) {
            $(`<ul class="ranking-content"><li>${1 * i + 1}</li><li>${user[i].Name}</li><li>${user[i].Score}</li><li>${user[i].Time}</li></ul>`).appendTo('.list-content');
        }
    }

    stop() {
        if (dataCenter.flag) {
            this.bgm.pause();
            $('.stopImg').css({background: "url('./img/pauseBtn.png')"});
            // 暂停定时器
            clearInterval(this.timer);
            clearInterval(this.check);
            $('.game-frame>*').css({
                animationPlayState: 'paused',
            })
        } else {
            this.tryToOpenMusic()
            $('.stopImg').css({background: "url('./img/stopBtn.png')"});
            this.intervalTimer();
            $('*').css({
                animationPlayState: 'running',
            })
        }
        dataCenter.flag = !dataCenter.flag;
    }

    musicPlay(src) {

        let music = $(`<audio src="${src}" autoplay onended="this.remove()"></audio>`);

        if (!this.muted) {
            music.appendTo('body')
        }

    }

    tip(txt) {
        $('<div class="tip">' + txt + '</div>').appendTo('.frame').one('animationend', function () {
            $('.tip').remove();
        });
    }

    checkPage(current, next, callback) {
        current.css({
            'animation':'frame-out 0.6s'
        }).one('animationend',()=>{
            current.hide();
            next.show().css({
                'animation':'frame-in 0.6s'
            }).one('animationend',callback);
        });
        // current.fadeOut(300, function () {
        //     next.fadeIn(300, callback)
        // })
    }
}

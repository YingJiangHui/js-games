let lastPoint = { x: undefined, y: undefined }
let newPonint = { x: undefined, y: undefined }
let drawingSurfaceImageData;
let rubberbandRect = {};
let canvas;
let mirror_flag = false;
let window_w = $(window).width();
let window_h = $(window).height();
let context_mirror;
let r, g, b;
let bool = {
    isSquare: false,
    reduce: false,
    plus: false,
    shift: false,
    space: false,
    move: false,
}
let context
let lineW = 1;
$(function() {
    $('.save-image').on('click', function(e) {
        exportCanvasAsPNG('canvas', 'canvas');
        e.stopPropagation()

    })
    $('.color').on('click', function(e) {
        $(".color-palette").fadeToggle();
        e.stopPropagation()
    })
    $('.pallet-input').on('input', function(e) {
        let _this = $(event.currentTarget);
        if (_this.val().length > 3) {
            _this.val(_this.val().substr(0, _this.val().length - 1));
        }
        if (parseInt(_this.val()) > 255) _this.val(255);
        _this.val(parseInt(_this.val()) || "");
        switch ($(this).attr('id')) {
            case "R":
                r = $(this).val();
                break;
            case "G":
                g = $(this).val();
                break;
            case "B":
                b = $(this).val();
                break;
        }
        $('.show-color').css({
            background: `rgb(${r||0},${g||0},${b||0})`,
        })

    })
    $('.color-palette').on('click', function(e) {
        e.stopImmediatePropagation(); //阻止事件冒泡
    })
    canvas = $('#canvas')[0];
    var state = null;
    [canvas.width, canvas.height] = [window_w, window_h];
    context = canvas.getContext('2d');
    var painting = false;
    $('.clear').on('click', function(e) {
        context.clearRect(0, 0, window_w, window_h);
        e.stopPropagation()
    })
    $('.tools>div').on('click', function(e) {
        e.stopPropagation()
        $(this).css({
            background: "yellow",
        }).siblings().css({
            background: '',
        })
        if (mirror_flag) {
            $('.mirror').css({ 'background': 'red', 'color': 'white' });
        }
        $('.color').css({
            background: `rgb(${r||0},${g||0},${b||0})`
        })
        state = $(this).attr('data-name');
    })
    $('.mirror').on('click', function(e) {
        e.stopPropagation()
        mirror_flag = !mirror_flag;
        if (mirror_flag) {
            $(this).css({ 'background': 'red', 'color': 'white' });
            creatMirror();
        } else {
            $(this).css({ 'background': '', 'color': '' });
            removeMirror();
        }
    })

    $('.clear').on('click', function(e) {
        e.stopPropagation()
        saveDrawingSurface();
        if (context_mirror)
            context_mirror.putImageData(drawingSurfaceImageData, 0, 0);
    })
    if (document.body.clientWidth >= 500) {
        $(window).on('mousedown', function(e) {
            mouseStart(e)
        })

        $(window).on('mousemove', function(e) {
            mouseMove(e)
        })
        $(window).on('mouseup', function(e) {
            mouseEnd(e)
        })
    } else {
        $(window).on('touchstart', function(e) {
            mouseStart(e)
        })
        $(window).on('touchend', function(e) {
            mouseEnd(e)
        })
        $(window).on('touchmove', function(e) { mouseMove(e) })
    }


    function mouseStart(e) {
        painting = true;
        let [x, y] = document.body.clientWidth <= 500 ? [e.originalEvent.changedTouches[0].clientX, e.originalEvent.changedTouches[0].clientY] : [e.clientX, e.clientY]
        lastPoint = { x, y };
        saveDrawingSurface();
        if (context.isPointInPath(lastPoint.x, lastPoint.y) && state == 'move') {
            bool.move = true;
        }
    }

    function mouseMove(e) {
        let [x, y] = document.body.clientWidth <= 500 ? [e.originalEvent.changedTouches[0].clientX, e.originalEvent.changedTouches[0].clientY] : [e.clientX, e.clientY]
        newPonint = { x, y };
        if (painting) {
            switch (state) {
                case 'pencil':
                    drawLine(lastPoint.x, lastPoint.y, newPonint.x, newPonint.y);
                    lastPoint = newPonint;
                    break;
                case 'circle':
                    restoreDrawingSurface();
                    drawCircle(lastPoint.x, lastPoint.y, distence(lastPoint.x, lastPoint.y, newPonint.x, newPonint.y));
                    break;
                case 'square':
                    restoreDrawingSurface();
                    drawSquare(newPonint.x, newPonint.y);
                    break;
                case 'rubber':
                    rubber(newPonint.x, newPonint.y);
                    break;
                case "move":
                    if (bool.move) {
                        restoreDrawingSurface();
                        moveElement();
                    } else {
                        restoreDrawingSurface();
                        choiceBox();
                    }
                    break;
            }
            saveMirror();
        }
    }

    function mouseEnd(e) {
        painting = false;
    }



    $('#color-done').on('click', function() {
        $('.color').css({
            background: `rgb(${r||0},${g||0},${b||0})`
        })
        context.strokeStyle = `rgb(${r||0},${g||0},${b||0})`;
        context.fillStyle = `rgb(${r||0},${g||0},${b||0})`;;

        $(".color-palette").fadeToggle();
    })
    $('.image').on('click', function() {
        $('#add-image').click();
    })
    $('#add-image').on('input', function(e) {
        let file = $(this)[0].files[0];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function() {
            imageLoad(reader.result).then((img) => {
                context.drawImage(img, canvas.width / 3 - img.width / 2, canvas.height / 3 - img.height / 2, img.width, img.height);
                saveDrawingSurface();

                saveMirror();
            }).then(() => {

            })
        }
    })
    $(window).on('keydown', function(e) {

        let key = e.keyCode
        switch (key) {
            case 17:
                bool.isSquare = true;
                break;
            case 16:
                bool.shift = true;
                break;
            case 189:
                bool.reduce = true;
                break;
            case 187:
                bool.plus = true;
                break;
            case 32:
                bool.space = !bool.space;
                break;

        }
        if (bool.plus && bool.shift) {
            context.lineWidth = ++lineW;
            line(lineW);
        } else if (bool.reduce && bool.shift) {
            context.lineWidth = --lineW <= 1 ? lineW = 1 : lineW;
            line(lineW);
        }
    })
    $(window).on('keyup', function(e) {
        let key = e.keyCode;
        switch (key) {
            case 17:
                bool.isSquare = false;
                break;
            case 16:
                bool.shift = false;
                break;
            case 189:
                bool.reduce = false;
                break;
            case 187:
                bool.plus = false;
                break;
        }
    })
})

function myIsNaN(value) {
    return (typeof value === 'number' && !isNaN(value));
}

function saveDrawingSurface() {
    drawingSurfaceImageData = context.getImageData(0, 0, canvas.width, canvas.height);
}

function restoreDrawingSurface() {
    context.putImageData(drawingSurfaceImageData, 0, 0);
}

function saveMirror() {
    var Mirror = context.getImageData(0, 0, canvas.width, canvas.height);

    if ($('#canvas-mirror').length != 0) {

        context_mirror.putImageData(Mirror, 0, 0);
    }

}

function distence(x1, y1, x2, y2) {
    var a = Math.abs(x1 - x2);
    var b = Math.abs(y1 - y2);
    var c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
    return c;
}

function drawLine(x1, y1, x2, y2) {
    context.beginPath();
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
    context.closePath();
}

function drawSquare(x, y) {

    context.beginPath();
    [rubberbandRect.width, rubberbandRect.height] = [Math.abs(x - lastPoint.x), Math.abs(y - lastPoint.y)];
    let minSide = Math.min(rubberbandRect.width, rubberbandRect.height);

    if (bool.isSquare) {
        [rubberbandRect.width, rubberbandRect.height] = [minSide, minSide];
    }
    if (x >= lastPoint.x) {
        rubberbandRect.left = lastPoint.x;
    } else {
        rubberbandRect.left = x;
        if (Math.abs(x - lastPoint.x) > Math.abs(y - lastPoint.y) && bool.isSquare) {
            rubberbandRect.left = lastPoint.x - minSide;
        }
    }
    if (y >= lastPoint.y) {
        rubberbandRect.top = lastPoint.y;
    } else {
        rubberbandRect.top = y;
        if (Math.abs(x - lastPoint.x) < Math.abs(y - lastPoint.y) && bool.isSquare) {
            rubberbandRect.top = lastPoint.y - minSide;
        }
    }

    context.rect(rubberbandRect.left, rubberbandRect.top, rubberbandRect.width, rubberbandRect.height);
    if (bool.space) {
        context.fill();
    } else {
        context.stroke();
    }
    context.closePath();
}

function choiceBox() {
    context.save();
    context.setLineDash([5, 15]);
    drawSquare(newPonint.x, newPonint.y);
    context.restore();
}

function moveElement() {
    context.beginPath();
    context.setLineDash([5, 15]);
    context.rect(newPonint.x - (rubberbandRect.width / 2), newPonint.y - (rubberbandRect.height / 2), rubberbandRect.width, rubberbandRect.height);
    context.stroke();
    context.closePath();
}

function drawCircle(x, y, r) {
    context.beginPath();
    context.arc(x, y, r, 0, Math.PI * 2);
    if (bool.space) {
        context.fill();
    } else {
        context.stroke();
    }
    context.closePath();
}

function rubber(x, y) {
    let rubber_canvas = $('<canvas></canvas>')[0];
    rubber_canvas.width = 20;
    rubber_canvas.height = 20;
    let rubber_context = rubber_canvas.getContext('2d');
    let rubber = rubber_context.getImageData(0, 0, 20, 20);
    context.putImageData(rubber, x - 10, y - 10);
    $(rubber_canvas).remove();
}

function creatMirror() {
    saveDrawingSurface();
    canvas.width = $(window).width() / 2 - 4;
    let canvasMirror = $('<canvas></canvas>').css({
        borderLeft: '1px solid black'
    }).attr('id', 'canvas-mirror')[0];
    canvasMirror.width = $(window).width() / 2 - 3;
    canvasMirror.height = $(window).height();
    $(canvasMirror).appendTo('body');
    context_mirror = $('#canvas-mirror')[0].getContext('2d');
    console.log(context_mirror)
    context.putImageData(drawingSurfaceImageData, 0, 0);
    context_mirror.putImageData(drawingSurfaceImageData, 0, 0);
}

function removeMirror() {
    saveDrawingSurface();
    $('#canvas-mirror').remove();
    canvas.width = $(window).width();
    context.putImageData(drawingSurfaceImageData, 0, 0);
}

function imageLoad(url) {
    return new Promise(function(resolve, reject) {
        let img = new Image();
        img.src = url;

        img.onload = function() {
            resolve(img);
            let w = 0,
                h = 0
            if (img.width > 1000 || img.height > 1000) {
                w = img.width / 10
                h = img.height / 10
            } else if (img.width > 500 || img.height > 500) {
                w = img.width / 5
                h = img.height / 5
            } else if (img.width > 200 || img.height > 200) {
                w = img.width / 2
                h = img.height / 2
            }
            img.width = w
            img.height = h
        }
    })
}

function line(w) {
    var showLineWidth;
    !showLineWidth ? showLineWidth = document.createElement('div') : showLineWidth;
    showLineWidth.innerHTML = 'line width : ' + w;
    showLineWidth.style.cssText = "position:absolute;right:20px;top: 70px;color : rgba(0,0,0,0.4);animation :moveTop 1s;top = 50 px;height :50 px;width : 200 px;";

    document.body.appendChild(showLineWidth);
    setTimeout(() => {
        document.body.removeChild(showLineWidth);
    }, 1000);
}

function exportCanvasAsPNG(id, fileName) {
    var canvasElement = document.getElementById(id);
    var MIME_TYPE = "image/png";
    var imgURL = canvasElement.toDataURL(MIME_TYPE);
    var dlLink = document.createElement('a');
    dlLink.download = fileName;
    dlLink.href = imgURL;
    dlLink.dataset.downloadurl = [MIME_TYPE, dlLink.download, dlLink.href].join(':');
    document.body.appendChild(dlLink);
    dlLink.click();
    document.body.removeChild(dlLink);
}
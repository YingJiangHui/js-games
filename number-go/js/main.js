$(function () {
    let map = [
        ["","",7,4,6,8,"",1,9],
        [8,1,9,"",2,7,4,"",3],
        [2,"",4,9,"",3,7,5,8],
        [9,4,"",3,7,1,"",2,6],
        [6,7,2,8,"",9,1,3,5],
        [1,"",8,2,5,6,9,"",4],
        [7,9,"",1,8,5,"",4,2],
        [5,"",1,6,9,4,3,8,7],
        [4,8,6,7,"","",5,9,1]
    ]
    // $.getJSON('number.json', (value) => {
    //     map = value.data;
    //     game();
    // });
    game();
    var reg = /^[1-9]d?$/, s = 1, m = 0, timer = null, flag = true

    function game() {
        loadGamePanel()
        $('.number_input').on('input', function (event) {
            let _this = $(event.currentTarget)
            if (reg.test(_this.val())) {

            } else {
                _this.val(_this.val().substring(0, _this.val().length - 1))
            }
            _this.attr('data-repeat', checkRepeat($(this)))
            winning()
            console.log(event.originalEvent.data)//按键输入的值
            console.log(_this==$(this))//false
            if (event.originalEvent.data==null){
                $(this).removeClass('choose')
            }
        })
        sameNumber()
    }

    //判断是否有相同值
    function checkRepeat(dom, loop = true) {
        let row = $(dom).attr('data-row'), col = $(dom).attr('data-col'), repeat = false, value = dom.val(),
            box = $(dom).attr('data-box');
        // console.log(value)
        $(`.number_input[data-row=${row}][data-col!=${col}],.number_input[data-col=${col}][data-row!=${row}],.number_input[data-box=${box}][data-row!=${row}][data-col!=${col}]`).each(function () {
            if (value == $(this).val()) {
                if (value == "") {
                    $(this).attr('data-repeat', false)
                } else {
                    repeat = true
                    $(this).attr('data-repeat', repeat)
                }
            } else if (loop) {
                if (!checkRepeat($(this), false)) {
                    $(this).attr('data-repeat', false)
                }
            }
        })
        return repeat
    }

    function winning() {
        console.log($(".number_input:valid:not([readonly])"))
        if ($(".number_input:not(:valid):not([readonly])").val() == undefined && $('.number_input[data-repeat=true]').val() == undefined) {
            clearInterval(timer)
            runking()
            $('.gamePanel').fadeOut(function () {
                $('.ranking').fadeIn(function () {
                    Alert('The Game You Won')
                })
            })
        }
    }

    //排行榜
    function runking() {
        var ranking = [];
        var time = $('.start:eq(1) :last-child').html(), use = $('.start:eq(0) :last-child').html()
        //localStorage.ranking存在就吧它转换为对象的形式，储存到ranking数组中去
        //如果不存在就自己存放到数组中
        if (localStorage.ranking) {
            ranking = JSON.parse(localStorage.ranking)
        }

        ranking.push({
            user: use,
            time: time
        });

        ranking.sort(sortRink);

        if (parseInt((time.toString().replace(":", ""))) <= parseInt(ranking[0].time.toString().replace(':', ''))) {
            // alert('新记录')
            Alert('New Records')
        }
        console.log(ranking.length)
        //把数组中的对对象转换为字符串的形式存放到localStorage.ranking 中
        $(".records-list").html('');
        //${rank[i].time}
        for (let i = 0; i < ranking.length; i++) {
            $(".records-list").append(`<ul class="records-table"><li>${i + 1}</li><li>${ranking[i].user}</li><li>${ranking[i].time}</li></ul>`);
        }
        localStorage.ranking = JSON.stringify(ranking)
    }

    //排序
    function sortRink(a, b) {
        //为0 表示相等//为1表示a大//为-1表示b大
        return parseInt(a.time.toString().replace(":", "")) - parseInt(b.time.toString().replace(':', ''))
    }

    //下载地图
    function loadGamePanel() {
        $('.number_go_input').html('');

        for (let row in map) {
            let dom_tr = $('<tr class="ng-tr"></tr>')
            for (let col in map[row]) {
                if (map[row][col]) {
                    $(`<td class="ng_grid"> <input class="number_input" data-row="${row}" data-col="${col}" readonly value="${map[row][col]}" ></td>`).appendTo(dom_tr)
                } else {
                    $(`<td class="ng_grid"><input class="number_input" data-row="${row}" required data-col="${col}"></td>`).appendTo(dom_tr)
                }
            }
            let number = 0
            $.each($('.number_input'), function () {
                //将字符串类型转换为number类型
                let row = parseInt($(this).attr('data-row'));
                let col = parseInt($(this).attr('data-col'));
                if (row % 3 == 0 && col % 3 == 0) {
                    for (let i = 0; i < 3; i++) {
                        for (let j = 0; j < 3; j++) {
                            $(`.number_input[data-row=${row + i}][data-col=${col + j}]`).attr('data-box', number)
                        }
                    }
                    number++
                }
            })
            dom_tr.appendTo('.number_go_input')
        }
    }

    //计时器
    function time() {
        clearInterval(timer)
        timer = setInterval(function () {

            if (s == 59) {
                m++
                s = 0
            }
            $('.start:eq(1) :last-child').text(`${m}:${s}`)
            s++
        }, 1000)
    }

    //提示框
    function Alert(txt) {
        $(`<div class="alert">${txt}</div>`).appendTo('body').one('animationend',function () {
            $(this).remove()
        })
    }

    //点击数字显示与其相同的所有数字
    function sameNumber() {
        $('.number_input').on('click',function () {
            var _this = $(this);
            $('.number_input').each(function () {
                if ($(this).val() == _this.val()) {
                    if (!(_this.val()=="")){
                        $(this).toggleClass('choose')
                    }
                    // let choose = !Boolean(($(this).attr('data-choose')));
                }
            })
        })
    }


    //赢得比赛切换排行版界面
    //开始游戏按钮
    $('.st_btn :first-child').on('click', function () {
        if ($('.input-label').val() !== "") {
            time()
            $('.start:eq(0) :last-child').text($('.input-label').val())
            $('.homePage').fadeOut(function () {
                $('.gamePanel').fadeIn(function () {

                })
            })
        } else {
            Alert('user is not defined')
        }

    })
    //暂停按钮
    $('.start-btn :first-child').on('click', function () {
        // $('.number_go_input').toggle(function () {
        //
        // })

        if (flag) {
            clearInterval(timer)
            $('.number_go_input').fadeOut()
            $(this).text("please")
        } else {
            time()
            $(this).text("stop")
            $('.number_go_input').fadeIn()
        }
        flag = !flag
    })
    //游戏返回主页按钮
    $('.start-btn :last-child').on('click', function () {
        $('.gamePanel').fadeOut(function () {
            window.location = location
        })
    })
    //查看游戏说明按钮
    $('.st_btn :last-child').on('click', function () {
        $('.homePage').fadeOut(function () {
        })
    })
    //排行榜返回按钮
    $('.runking-btn :last-child').on('click', function () {
        $('.ranking').fadeOut(function () {
            window.location = location
        })
    })
    //排行榜开始游戏按钮
    $('.runking-btn :first-child').on('click', function () {
        s=0
        m=0
        game()
        $('.ranking').fadeOut(function () {
            $('.gamePanel').css({
                opacity: '1'
            }).show(function () {
                time()
            })
        })
    })
})